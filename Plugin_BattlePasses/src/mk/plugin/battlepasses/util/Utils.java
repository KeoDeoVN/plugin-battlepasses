package mk.plugin.battlepasses.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import mk.plugin.battlepasses.battlepass.BattlePass;
import mk.plugin.battlepasses.battlepass.BattlePasses;
import mk.plugin.battlepasses.mission.MissionGroup;
import mk.plugin.battlepasses.player.BPPlayer;
import mk.plugin.battlepasses.player.BPPlayers;
import mk.plugin.battlepasses.quest.QuestUtils;

public class Utils {
	
	public static int caculateMaxPoint(String id) {
		BattlePass bp = BattlePasses.get(id);
		long start = bp.getTimeStart();
		long period = bp.getTimeEnd() - start;
		int sum = 0;
		for (Entry<String, MissionGroup> mgs : bp.getMissionGroups().entrySet()) {
			MissionGroup mg = mgs.getValue();
			int times = new Long(period % mg.getInterval() == 0 ? period / mg.getInterval() : period / mg.getInterval() + 1).intValue();
			sum += mg.getPoint() * times * mg.getRandomAmount();
		}
		return sum;
	}
	
	public static long getRemain(Player player, String quest) {
		BPPlayer bpP = BPPlayers.get(player);
		String bpID = bpP.getBattlePassID();
		String mID = QuestUtils.getMission(quest, bpID);
		BattlePass bp = BattlePasses.get(bpID);
		MissionGroup mg = bp.getMissionGroup(mID);
		
		long sub = mg.getInterval() - (System.currentTimeMillis() - (bp.getTimeStart() + mg.getInterval() * bpP.getMissionCount(mID)));
		long remain = sub % mg.getInterval();
		
		return remain;
	}
	
	public static String format(long miliTime) {
		return (miliTime / (3600000 * 24)) + "d " + ((miliTime % (3600000 * 24)) / 3600000) + "h " + ((miliTime % 3600000) / 60000) + "m " + ((miliTime % 60000) / 1000) + "s";
	}
	
	public static long getRemain(String bpID) {
		BattlePass bp = BattlePasses.get(bpID);
		return bp.getTimeEnd() - System.currentTimeMillis();
	}
	
	public static ItemStack getBlackSlot() {
		ItemStack is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
		ItemStackUtils.setDisplayName(is, "§r");
		return is;
	}
	
	public static ItemStack getBarrierSlot() {
		ItemStack is = new ItemStack(Material.BARRIER, 1);
		ItemStackUtils.setDisplayName(is, "§r");
		return is;
	}
	
	@SuppressWarnings("deprecation")
	public static short getColor(DyeColor color) {
		return color.getWoolData();
	}
	
	public static List<String> toList(String s, int length, String start) {
		List<String> result = new ArrayList<String>();
		if (s == null)
			return result;
		if (!s.contains(" ")) {
			result.add(s);
			return result;
		}

		String[] words = s.split(" ");
		int l = 0;
		String line = "";
		for (int i = 0; i < words.length; i++) {
			l += words[i].length();
			if (l > length) {
				result.add(line.substring(0, line.length() - 1));
				l = words[i].length();
				line = "";
				line += words[i] + " ";
			} else {
				line += words[i] + " ";
			}
		}

		if (!line.equalsIgnoreCase(" "))
			result.add(line);

		for (int i = 0; i < result.size(); i++) {
			result.set(i, start + result.get(i));
		}

		return result;
	}
	
}
