package mk.plugin.battlepasses.task;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Maps;

import me.blackvein.quests.Quest;
import me.blackvein.quests.Quester;
import mk.plugin.battlepasses.battlepass.BattlePassData;
import mk.plugin.battlepasses.battlepass.BattlePasses;
import mk.plugin.battlepasses.manager.Timings;
import mk.plugin.battlepasses.player.BPPlayer;
import mk.plugin.battlepasses.player.BPPlayers;
import mk.plugin.battlepasses.quest.QuestUtils;

public class TimingTask extends BukkitRunnable {
	
	public TimingTask(Plugin plugin) {
		this.runTaskTimer(plugin, 0, 20);
	}
	
	@Override
	public void run() {
		// Check BattlePasses
		BattlePasses.get().forEach((bpID, bp) -> {
			// Check season
			if (Timings.isEnded(bpID)) {
				Timings.clearData(bpID);
				return;
			}
			Timings.checkMissionIntervals(bpID);
		});
		
		// Check Players
		Bukkit.getOnlinePlayers().forEach(player -> {
			// Check season
			if (!check(player)) return;
			Timings.checkPlayerMissions(player);
		});
	}
	
	public boolean check(Player player) {
		BPPlayer bpP = BPPlayers.get(player);
		String bpID = bpP.getBattlePassID();
		if (bpID == null) return false;
		
		
		
		// Check end & remove data
		if (!Timings.isInSeason(bpID)) {
			// Remove quests
			BattlePasses.get().values().forEach(bp -> {
				bp.getMissionGroups().values().forEach(mg -> {
					mg.getQuests().forEach(quest -> QuestUtils.failQuest(player, quest));
				});
			});

			
			// Remove data
			bpP.clearBattlePassData(false);
			BPPlayers.save(player, bpP);
			return false;
		}
		// Give quests
		else {
			Quester quester = QuestUtils.getQuestsPlugin().getQuester(player.getUniqueId());
			Map<Quest, Integer> currents = Maps.newHashMap(quester.getCurrentQuests());
			BattlePassData bpd = BattlePasses.getData(bpID);
			bpd.getActiveMissions().forEach((type, list) -> {
				list.forEach(qname -> {
					Quest q = QuestUtils.getQuestsPlugin().getQuest(qname);
					if (!currents.containsKey(q) && !BPPlayers.isComplete(player, qname)) {
						QuestUtils.takeQuest(player, qname);
					}
				});
			});
		}
		
		return true;
	}
	
}
