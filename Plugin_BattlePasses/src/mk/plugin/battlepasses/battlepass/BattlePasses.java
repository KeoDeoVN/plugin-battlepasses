package mk.plugin.battlepasses.battlepass;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.google.common.collect.Maps;

import mk.plugin.battlepasses.battlepass.presale.PreSale;
import mk.plugin.battlepasses.eco.EcoType;
import mk.plugin.battlepasses.gui.missions.GUIMission;
import mk.plugin.battlepasses.gui.presale.GUIPreSale;
import mk.plugin.battlepasses.level.Level;
import mk.plugin.battlepasses.manager.Timings;
import mk.plugin.battlepasses.mission.MissionGroup;
import mk.plugin.battlepasses.player.BPPlayer;
import mk.plugin.battlepasses.player.BPPlayers;
import mk.plugin.battlepasses.reward.Reward;
import mk.plugin.playerdata.storage.GlobalData;
import mk.plugin.playerdata.storage.PlayerDataAPI;

public class BattlePasses {
	
	private static final String KEY = "battlePass-%id%";
	
	private static Map<String, BattlePass> map = Maps.newHashMap();
	
	public static void loadAll(FileConfiguration config) {
		map.clear();
		config.getConfigurationSection("battlepass").getKeys(false).forEach(id -> {
			// 
			String path = "battlepass." + id;
			String name = config.getString(path + ".name");
			long timeStart = config.getLong(path + ".time-start");
			long timeEnd = config.getLong(path + ".time-end");
			
			// Pre-sale
			EcoType eco = EcoType.valueOf(config.getString(path + ".pre-sale.price").split(":")[0]);
			int price = Integer.valueOf(config.getString(path + ".pre-sale.price").split(":")[1]);
			int slot = config.getInt(path + ".pre-sale.slot");
			List<String> desc = config.getStringList(path + ".pre-sale.desc").stream().map(s -> s.replace("&", "§")).collect(Collectors.toList());
			PreSale preSale = new PreSale(eco, price, slot, desc);
			
			// Missions
			Map<String, MissionGroup> missionGroups = Maps.newHashMap();
			for (String mID : config.getConfigurationSection(path + ".mission-group").getKeys(false)) {
				String path2 = path + ".mission-group." + mID;
				long interval = config.getLong(path2 + ".interval");
				int point = config.getInt(path2 + ".point");
				List<Integer> slots = config.getStringList(path2 + ".slots").stream().map(Integer::valueOf).collect(Collectors.toList());
				int randomAmount = config.getInt(path2 + ".random-amount");
				List<String> quests = config.getStringList(path2 + ".quests");
				
				missionGroups.put(mID, new MissionGroup(interval, randomAmount, point, slots, quests));
			}
			
			// Levels
			Map<Integer, Level> levels = Maps.newHashMap();
			config.getStringList(path + ".level").forEach(s -> {
				int level = Integer.valueOf(s.split(";")[0]);
				int pointRequired = Integer.valueOf(s.split(";")[1]);
				String reward = s.split(";")[2];
				boolean special = false;
				boolean isPercent = false;
				if (s.split(";").length >= 4) special = Boolean.valueOf(s.split(";")[3]); 
				if (s.split(";").length >= 5) isPercent = Boolean.valueOf(s.split(";")[4]); 
				levels.put(level, new Level(pointRequired, isPercent, reward, special));
			});;
			
			// Rewards
			Map<String, Reward> rewards = Maps.newHashMap();;
			config.getConfigurationSection(path + ".reward").getKeys(false).forEach(rID -> {
				List<String> cmds = config.getStringList(path + ".reward." + rID + ".cmds");
				List<String> rdesc = config.getStringList(path + ".reward." + rID + ".desc");
				rewards.put(rID, new Reward(cmds, rdesc));
			});
			
			// New
			map.put(id, new BattlePass(name, timeStart, timeEnd, preSale, missionGroups, levels, rewards));
		});
	}
	
	public static Map<String, BattlePass> get() {
		return map;
	}
	
	public static BattlePass get(String id) {
		return map.getOrDefault(id, null);
	}
	
	public static BattlePassData getData(String id) {
		GlobalData gd = PlayerDataAPI.getGlobalData();
		String key = KEY.replace("%id%", id);
		if (gd.hasData(key)) return BattlePassData.fromString(gd.getValue(key));
		return new BattlePassData();
	}
	
	public static void saveData(String id, BattlePassData bpData) {
		GlobalData gd = PlayerDataAPI.getGlobalData();
		String key = KEY.replace("%id%", id);
		gd.set(key, bpData.toString());
		PlayerDataAPI.saveGlobalData();
	}
	
	public static void removeData(String id) {
		String key = KEY.replace("%id%", id);
		GlobalData gd = PlayerDataAPI.getGlobalData();
		gd.remove(key);
		PlayerDataAPI.saveGlobalData();
	}
	
	public static void open(Player player) {
		BPPlayer bp = BPPlayers.get(player);
		if (bp.getBattlePassID() == null || !Timings.isInSeason(bp.getBattlePassID())) {
			GUIPreSale.open(player);
		} else GUIMission.open(player, bp.getBattlePassID());
	}
	
}
