package mk.plugin.battlepasses.battlepass.presale;

import java.util.List;

import org.bukkit.entity.Player;

import mk.plugin.battlepasses.eco.EcoType;

public class PreSale {
	
	private EcoType eco;
	private int price;
	private int slot;
	private List<String> desc;
	
	public PreSale(EcoType eco, int price, int slot, List<String> desc) {
		this.eco = eco;
		this.price = price;
		this.slot = slot;
		this.desc = desc;
	}
	
	public EcoType getEco() {
		return this.eco;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public int getSlot() {
		return this.slot;
	}
	
	public List<String> getDesc() {
		return this.desc;
	}
	
	public void cost(Player player) {
		eco.take(player, this.price);
	}
	
}
