package mk.plugin.battlepasses.battlepass.presale;

import org.bukkit.entity.Player;

import mk.plugin.battlepasses.player.BPPlayer;
import mk.plugin.battlepasses.player.BPPlayers;

public class PreSales {
	
	public static PreSaleStatus getStatus(Player player, String id) {
		BPPlayer bpP = BPPlayers.get(player);
		if (bpP.getBattlePassID() != null && bpP.getBattlePassID().equals(id)) return PreSaleStatus.BOUGHT;
		return PreSaleStatus.AVAILABLE;
	}
	
}
