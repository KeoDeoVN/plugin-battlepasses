package mk.plugin.battlepasses.battlepass.presale;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import mk.plugin.battlepasses.battlepass.BattlePass;
import mk.plugin.battlepasses.battlepass.BattlePasses;
import mk.plugin.battlepasses.util.ItemStackUtils;
import mk.plugin.battlepasses.util.Utils;

public enum PreSaleStatus {
	
	BOUGHT {
		@Override
		public ItemStack getIcon(String id) {
			BattlePass bp = BattlePasses.get(id);
			long remain = bp.getTimeStart() - System.currentTimeMillis();
			ItemStack is = new ItemStack(Material.BOOK, 1);
			ItemStackUtils.setDisplayName(is, "§6§lSổ " + bp.getName());
			List<String> lore = Lists.newArrayList();
			lore.addAll(bp.getPreSale().getDesc());
			lore.add("");
			lore.add("§aThời gian mở sổ: §f" + Utils.format(remain));
			ItemStackUtils.setLore(is, lore);
			ItemStackUtils.addEnchantEffect(is);
			
			return is;
		}
	},
	
	AVAILABLE {
		@Override
		public ItemStack getIcon(String id) {
			BattlePass bp = BattlePasses.get(id);
			long remain = bp.getTimeStart() - System.currentTimeMillis();
			ItemStack is = new ItemStack(Material.BOOK, 1);
			ItemStackUtils.setDisplayName(is, "§6§lSổ " + bp.getName());
			List<String> lore = Lists.newArrayList();
			lore.addAll(bp.getPreSale().getDesc());
			lore.add("");
			lore.add("§aGiá: " + bp.getPreSale().getEco().getColor() + bp.getPreSale().getPrice() + " " + bp.getPreSale().getEco().getName());
			lore.add("§aThời gian cày: §f" + Utils.format(bp.getTimeEnd() - bp.getTimeStart()));
			if (remain > 0) {
				lore.add("§aThời gian mở: §f" + Utils.format(remain));
			} else lore.add("§aThời gian mở: §2Đã mở");
			
			ItemStackUtils.setLore(is, lore);
			
			return is;
		}
	}
	
	;
	
	public abstract ItemStack getIcon(String id);
	
}
