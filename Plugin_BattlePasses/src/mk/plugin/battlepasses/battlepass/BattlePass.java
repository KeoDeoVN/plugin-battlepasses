package mk.plugin.battlepasses.battlepass;

import java.util.Map;

import mk.plugin.battlepasses.battlepass.presale.PreSale;
import mk.plugin.battlepasses.level.Level;
import mk.plugin.battlepasses.mission.MissionGroup;
import mk.plugin.battlepasses.reward.Reward;

public class BattlePass {
	
	private String name;
	private long timeStart;
	private long timeEnd;
	private PreSale preSale;
	private Map<String, MissionGroup> missionGroups;
	private Map<Integer, Level> levels;
	private Map<String, Reward> rewards;
	
	public BattlePass(String name, long timeStart, long timeEnd, PreSale preSale, Map<String, MissionGroup> missionGroups, Map<Integer, Level> levels, Map<String, Reward> rewards) {
		this.name = name;
		this.timeStart = timeStart;
		this.timeEnd = timeEnd;
		this.preSale = preSale;
		this.missionGroups = missionGroups;
		this.levels = levels;
		this.rewards = rewards;
	}
	
	public String getName() {
		return this.name;
	}
	
	public long getTimeStart() {
		return this.timeStart;
	}
	
	public long getTimeEnd() {
		return this.timeEnd;
	}
	
	public PreSale getPreSale() {
		return this.preSale;
	}
	
	public Map<String, MissionGroup> getMissionGroups() {
		return this.missionGroups;
	}
	
	public MissionGroup getMissionGroup(String id) {
		return this.missionGroups.getOrDefault(id, null);
	}

	public Map<Integer, Level> getLevels() {
		return levels;
	}

	public Level getLevel(int level) {
		return this.levels.getOrDefault(level, null);
	}
	
	public Map<String, Reward> getRewards() {
		return rewards;
	}
	
	public Reward getReward(String rewardID) {
		return this.rewards.getOrDefault(rewardID, null);
	}
	
	
}
