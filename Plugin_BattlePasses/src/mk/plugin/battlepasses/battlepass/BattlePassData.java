package mk.plugin.battlepasses.battlepass;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.google.common.collect.Maps;

public class BattlePassData {
	
	private Map<String, Long> missionCounts;
	private Map<String, List<String>> activeMissions;
	
	public BattlePassData() {
		this.missionCounts = Maps.newHashMap();
		this.activeMissions = Maps.newHashMap();
	}
	
	public BattlePassData(Map<String, Long> missionCounts, Map<String, List<String>> activeMissions) {
		this.missionCounts = missionCounts;
		this.activeMissions = activeMissions;
	}
	
	public Map<String, Long> getMissionCounts() {
		return this.missionCounts;
	}
	
	public long getMissionCount(String mID) {
		return this.missionCounts.getOrDefault(mID, 0l);
	}
	
	public void setMissionCount(String mID, long count) {
		this.missionCounts.put(mID, count);
	}
	
	public Map<String, List<String>> getActiveMissions() {
		return this.activeMissions;
	}
	
	public void setActiveMissions(String mID, List<String> quests) {
		Collections.sort(quests);
		this.activeMissions.put(mID, quests);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		JSONObject jo = new JSONObject();
		
		jo.put("missionCounts", new JSONObject(this.missionCounts));
		jo.put("activeMissions", new JSONObject(this.activeMissions));
		
		return jo.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public static BattlePassData fromString(String s) {
		JSONObject jo = (JSONObject) JSONValue.parse(s);
		
		Map<String, Long> missionCounts = (JSONObject) jo.get("missionCounts");
		Map<String, List<String>> activeMissions = (JSONObject) jo.get("activeMissions");
		
		return new BattlePassData(missionCounts, activeMissions);
	}
	
}
