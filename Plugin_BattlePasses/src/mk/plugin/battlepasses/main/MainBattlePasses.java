package mk.plugin.battlepasses.main;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import mk.plugin.battlepasses.battlepass.BattlePasses;
import mk.plugin.battlepasses.command.AdminCommand;
import mk.plugin.battlepasses.command.PlayerCommand;
import mk.plugin.battlepasses.config.Configs;
import mk.plugin.battlepasses.listener.GUIListener;
import mk.plugin.battlepasses.listener.QuestListener;
import mk.plugin.battlepasses.task.TimingTask;
import mk.plugin.battlepasses.yaml.YamlFile;

public class MainBattlePasses extends JavaPlugin {
	
	@Override
	public void onEnable() {
		this.reloadConfigs();
		this.registerCommands();
		this.registerListeners();
		this.regsiterTasks();
	}
	
	public void reloadConfigs() {
		this.saveDefaultConfig();
		YamlFile.reloadAll(this);
		Configs.reload(YamlFile.CONFIG.get());
		BattlePasses.loadAll(YamlFile.CONFIG.get());
	}
	
	public void registerCommands() {
		this.getCommand("battlepassadmin").setExecutor(new AdminCommand());
		this.getCommand("battlepass").setExecutor(new PlayerCommand());
	}
	
	public void registerListeners() {
		Bukkit.getPluginManager().registerEvents(new QuestListener(), this);
		Bukkit.getPluginManager().registerEvents(new GUIListener(), this);
	}
	
	public void regsiterTasks() {
		new TimingTask(this);
	}
	
	public static MainBattlePasses get() {
		return MainBattlePasses.getPlugin(MainBattlePasses.class);
	}
	
}
