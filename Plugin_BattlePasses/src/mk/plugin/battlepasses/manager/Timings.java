package mk.plugin.battlepasses.manager;

import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

import org.bukkit.entity.Player;

import com.google.common.collect.Lists;

import mk.plugin.battlepasses.battlepass.BattlePass;
import mk.plugin.battlepasses.battlepass.BattlePassData;
import mk.plugin.battlepasses.battlepass.BattlePasses;
import mk.plugin.battlepasses.mission.MissionGroup;
import mk.plugin.battlepasses.player.BPPlayer;
import mk.plugin.battlepasses.player.BPPlayers;
import mk.plugin.battlepasses.quest.QuestUtils;

public class Timings {
	
	public static boolean isInSeason(String bpID) {
		BattlePass bp = BattlePasses.get(bpID);
		return bp.getTimeEnd() >= System.currentTimeMillis() && bp.getTimeStart() <= System.currentTimeMillis();
	}
	
	public static boolean isEnded(String bpID) {
		BattlePass bp = BattlePasses.get(bpID);
		return bp.getTimeEnd() < System.currentTimeMillis();
	}
	
	public static void clearData(String bpID) {
		BattlePasses.removeData(bpID);
	}
	
	public static void checkMissionIntervals(String bpID) {
		if (isEnded(bpID)) {
			clearData(bpID);
			return;
		}
		
		boolean change = false;
		BattlePass bp = BattlePasses.get(bpID);
		BattlePassData bpData = BattlePasses.getData(bpID);
		
		long currentTime = System.currentTimeMillis();
		for (Entry<String, MissionGroup> e : bp.getMissionGroups().entrySet()) {
			String mID = e.getKey();
			MissionGroup mg = e.getValue();
			
			long count = currentTime / mg.getInterval();
			
			// Check conditions
			if (bpData.getMissionCount(mID) >= count) continue;
			
			// Reset
			List<String> quests = random(bp.getMissionGroup(mID).getQuests(), bp.getMissionGroup(mID).getRandomAmount());
			bpData.setActiveMissions(mID, quests);
			bpData.setMissionCount(mID, count);
			
			// Change
			change = true;
		}
		
		// Check change
		if (change) {
			// Save
			BattlePasses.saveData(bpID, bpData);
		}

	}
	
	/*
	 * Check mission quests
	 * Reset if mission has new count
	 */
	
	public static void checkPlayerMissions(Player player) {
		boolean change = false;
		
		BPPlayer bpP = BPPlayers.get(player);
		if (bpP.getBattlePassID() == null) return;
		BattlePassData bpData = BattlePasses.getData(bpP.getBattlePassID());
		BattlePass bp = BattlePasses.get(bpP.getBattlePassID());
		
		long currentTime = System.currentTimeMillis();
		if (bpP.getMissionCounts().size() == 0) {
			bp.getMissionGroups().keySet().forEach(mID -> {
				bpP.setMissionCount(mID, -1);
			});
		}
		
		for (Entry<String, Long> e : Lists.newArrayList(bpP.getMissionCounts().entrySet())) {
			String mID = e.getKey();
			long currentCount = e.getValue();
			
			MissionGroup mg = bp.getMissionGroup(mID);
			long count = (currentTime - bp.getTimeStart()) / mg.getInterval();
			
			// Check conditions
			if (currentCount >= count) continue;
			
			// Set count
			bpP.setMissionCount(mID, count);
			
			// Check quests
			List<String> quests = mg.getQuests();
			List<String> newQuests = bpData.getActiveMissions().get(mID);
			
			// Remove old quests
			quests.forEach(quest -> {
				QuestUtils.failQuest(player, quest);
			});
			
			// Add new quests
			newQuests.forEach(quest -> {
				QuestUtils.takeQuest(player, quest);
			});
			
			// Remove completed quests
			bpP.setCompletedQuest(mID, Lists.newArrayList());
			
			// Send message
			player.sendMessage("§aNhiệm vụ sổ sứ mệnh đã được reset! Hãy kiểm tra nhiệm vụ mới");
			
			// Change
			change = true;
		}
		
		// Check change
		if (change) BPPlayers.save(player, bpP);
	}
	
	private static List<String> random(List<String> list, int amount) {
		if (amount > list.size()) return list;
		List<String> result = Lists.newArrayList();
		List<String> clone = Lists.newArrayList(list);
		int c = 0;
		while (c < amount) {
			c++;
			int index = new Random().nextInt(clone.size());
			String content = clone.get(index);
			result.add(content);
			clone.remove(index);
		}
		return result;
	}
	
	
	
	
	
	
}
