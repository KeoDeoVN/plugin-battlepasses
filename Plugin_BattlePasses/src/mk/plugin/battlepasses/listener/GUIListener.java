package mk.plugin.battlepasses.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import mk.plugin.battlepasses.gui.levels.GUILevel;
import mk.plugin.battlepasses.gui.missions.GUIMission;
import mk.plugin.battlepasses.gui.presale.GUIConfirm;
import mk.plugin.battlepasses.gui.presale.GUIPreSale;

public class GUIListener implements Listener {

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		GUILevel.onClick(e);
		GUIMission.onClick(e);
		GUIConfirm.onClick(e);
		GUIPreSale.onClick(e);
	}
	
}
