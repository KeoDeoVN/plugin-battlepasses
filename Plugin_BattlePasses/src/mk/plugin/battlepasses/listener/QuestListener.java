package mk.plugin.battlepasses.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import me.blackvein.quests.events.quester.QuesterPostCompleteQuestEvent;
import mk.plugin.battlepasses.battlepass.BattlePass;
import mk.plugin.battlepasses.battlepass.BattlePasses;
import mk.plugin.battlepasses.manager.Timings;
import mk.plugin.battlepasses.mission.MissionGroup;
import mk.plugin.battlepasses.player.BPPlayer;
import mk.plugin.battlepasses.player.BPPlayers;
import mk.plugin.battlepasses.quest.QuestUtils;

public class QuestListener implements Listener {
	
	@EventHandler
	public void onCompleteQuest(QuesterPostCompleteQuestEvent e) {
		String quest = e.getQuest().getName();
		Player player = e.getQuester().getPlayer();
		BPPlayer bpP = BPPlayers.get(player);
		if (bpP == null) return;
		if (bpP.getBattlePassID() == null || !Timings.isInSeason(bpP.getBattlePassID())) return;
		
		// Set quests
		bpP.addCompletedQuest(quest);
		
		// Add point
		String mID = QuestUtils.getMission(e.getQuest().getName(), bpP.getBattlePassID());
		if (mID == null) return;
		BattlePass bp = BattlePasses.get(bpP.getBattlePassID());
		MissionGroup mg = bp.getMissionGroup(mID);
		bpP.setPoint(bpP.getPoint() + mg.getPoint());
		
		// Save
		BPPlayers.save(player, bpP);
	}
	
}
