package mk.plugin.battlepasses.player;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import mk.plugin.battlepasses.battlepass.BattlePass;
import mk.plugin.battlepasses.battlepass.BattlePasses;
import mk.plugin.battlepasses.mission.MissionGroup;

public class BPPlayer {
	
	private String bpID;
	private int bpPoint;
	private int bpLevel;
	private int pointBought;
	
	/*
	 * Key: Mission ID
	 * Value: Quest Name
	 * Quests in map will be reset with their mission
	 */
	private Map<String, List<String>> completedQuests;
	
	/*
	 * Count: counting the amount of intervals
	 * so that system can indentify and add/remove quests 
	 */
	private Map<String, Long> missionCounts;
	
	public BPPlayer() {
		this.bpID = null;
		this.bpPoint = 0;
		this.bpLevel = 0;
		this.completedQuests = Maps.newHashMap();
		this.missionCounts = Maps.newHashMap();
	}
	
	public BPPlayer(String bpID, int bpPoint, int bpLevel, int pointBought, Map<String, List<String>> completedQuests, Map<String, Long> missionCounts) {
		this.bpID = bpID;
		this.bpPoint = bpPoint;
		this.bpLevel = bpLevel;
		this.pointBought = pointBought;
		this.completedQuests = completedQuests;
		this.missionCounts = missionCounts;
	}
	
	public void clearBattlePassData(boolean removeID) {
		if (removeID) this.bpID = null;
		this.completedQuests = Maps.newHashMap();
		this.missionCounts = Maps.newHashMap();
	}
	
	public String getBattlePassID() {
		return this.bpID;
	}
	
	public void setBattlePassID(String bpID, boolean clearOld) {
		this.bpID = bpID;
		if (clearOld) {
			this.bpLevel = 0;
			this.bpPoint = 0;
			this.completedQuests = Maps.newHashMap();
			this.missionCounts = Maps.newHashMap();
		}
	}
	
	public int getPoint() {
		return this.bpPoint;
	}
	
	public void setPoint(int point) { 
		this.bpPoint = point;
	}
	
	public Map<String, List<String>> getCompletedQuests() {
		return this.completedQuests;
	}
	
	public List<String> getCompletedQuests(String mID) {
		return this.completedQuests.getOrDefault(mID, Lists.newArrayList());
	}
	
	public void addCompletedQuest(String mID, String quest) {
		List<String> l = getCompletedQuests(mID);
		l.add(quest);
		this.completedQuests.put(mID, l);
	}
	
	public void addCompletedQuest(String quest) {
		// Check mission id
		BattlePass bp = BattlePasses.get(bpID);
		String mID = null;
		if (bp == null) return;
		for (Entry<String, MissionGroup> mg : bp.getMissionGroups().entrySet()) {
			if (mg.getValue().getQuests().contains(quest)) {
				mID = mg.getKey();
				break;
			}
		}
		if (mID == null) return;
		
		// Add
		addCompletedQuest(mID, quest);
	}
	
	public void setCompletedQuest(String mID, List<String> quests) {
		this.completedQuests.put(mID, quests);
	}
	
	public long getMissionCount(String mID) {
		return this.missionCounts.getOrDefault(mID, null);
	}
	
	public Map<String, Long> getMissionCounts() {
		return this.missionCounts;
	}
	
	public void setMissionCount(String mID, long count) {
		this.missionCounts.put(mID, count);
	}
	
	public int getLevel() {
		return this.bpLevel;
	}
	
	public void setLevel(int level) {
		this.bpLevel = level;
	}
	
	public int getPointBought() {
		return this.pointBought;
	}
	
	public void setPointBought(int point) {
		this.pointBought = point;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		JSONObject jo = new JSONObject();
		
		jo.put("bpID", this.bpID);
		jo.put("bpPoint", this.bpPoint);
		jo.put("bpLevel", this.bpLevel);
		jo.put("completedQuests", new JSONObject(this.completedQuests));
		jo.put("missionCounts", this.missionCounts);
		jo.put("pointBought", this.pointBought);
		
		return jo.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public static BPPlayer fromString(String s) {
		JSONObject jo = (JSONObject) JSONValue.parse(s);
		
		String bpID = (String) jo.get("bpID");
		int bpPoint = new Long((long) jo.get("bpPoint")).intValue();
		int bpLevel = new Long((long) jo.get("bpLevel")).intValue();
		Map<String, List<String>> completedQuests = (JSONObject) jo.get("completedQuests");
		Map<String, Long> missionCounts = (JSONObject) jo.get("missionCounts");
		int pointBought = jo.containsKey("pointBought") ? new Long((long) jo.get("pointBought")).intValue() : 0;
		
		return new BPPlayer(bpID, bpPoint, bpLevel, pointBought, completedQuests, missionCounts);	
	}

	
}
