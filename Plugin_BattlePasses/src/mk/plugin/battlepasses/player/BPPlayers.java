package mk.plugin.battlepasses.player;

import java.util.List;
import java.util.Map.Entry;

import org.bukkit.entity.Player;

import mk.plugin.playerdata.storage.PlayerData;
import mk.plugin.playerdata.storage.PlayerDataAPI;

public class BPPlayers {
	
	private static final String KEY = "battlepass";
	
	public static BPPlayer get(String player) {
		PlayerData pd = PlayerDataAPI.get(player, "battlepasses");
		if (pd.hasData(KEY)) return BPPlayer.fromString(pd.getValue(KEY));
		return new BPPlayer();
	}
	
	public static BPPlayer get(Player player) {
		return get(player.getName());
	}
	
	public static void save(String player, BPPlayer data) {
		PlayerData pd = PlayerDataAPI.get(player, "battlepasses");
		pd.set(KEY, data.toString());
		pd.save();
	}
	
	public static void save(Player player, BPPlayer data) {
		save(player.getName(), data);
	}
	
	public static void remove(String player) {
		PlayerData pd = PlayerDataAPI.get(player, "battlepasses");
		pd.remove(KEY);
		pd.save();
	}
	
	public static boolean isComplete(Player player, String questName) {
		BPPlayer bpP = get(player);
		for (Entry<String, List<String>> c : bpP.getCompletedQuests().entrySet()) {
			if (c.getValue().contains(questName)) return true;
 		}
		return false;
	}
	
}
