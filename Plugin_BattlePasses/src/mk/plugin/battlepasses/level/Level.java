package mk.plugin.battlepasses.level;

import mk.plugin.battlepasses.battlepass.BattlePasses;
import mk.plugin.battlepasses.reward.Reward;
import mk.plugin.battlepasses.util.Utils;

public class Level {
	
	private int pointRequired;
	private boolean isPercent;
	private String reward;
	private boolean special;
	
	public Level(int pointRequired, boolean isPercent, String reward, boolean special) {
		this.pointRequired = pointRequired;
		this.isPercent = isPercent;
		this.reward = reward;
		this.special = special;
	}
	
	public boolean isSpecial() {
		return this.special;
	}
	
	public boolean isPercent() {
		return this.isPercent;
	}
	
	public int getPointRequired(String bpID) {
		if (!this.isPercent) return this.pointRequired;
		int max = Utils.caculateMaxPoint(bpID);
		return max * this.pointRequired / 100;
	}
	
	public String getReward() {
		return this.reward;
	}
	
	public Reward getReward(String bpID) {
		return BattlePasses.get(bpID).getReward(this.reward);
	}
	
}
