package mk.plugin.battlepasses.quest;

import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import me.blackvein.quests.CustomObjective;
import me.blackvein.quests.Quest;
import me.blackvein.quests.Quester;
import me.blackvein.quests.Quests;
import me.clip.placeholderapi.PlaceholderAPI;
import mk.plugin.battlepasses.util.ItemStackUtils;
import mk.plugin.battlepasses.util.Utils;
import mk.plugin.playerdata.storage.PlayerData;
import mk.plugin.playerdata.storage.PlayerDataAPI;

public enum QuestStatus {
	
	COOLDOWN {
		@Override
		public ItemStack getIcon(Player player, String quest, int point) {
			ItemStack item = new ItemStack(Material.BOOK);
			ItemStackUtils.setDisplayName(item, "§6§l" + quest);
			ItemStackUtils.addLoreLine(item, "§aĐiểm: §f" + point);
			ItemStackUtils.addLoreLine(item, "§aReset sau: §7" + Utils.format(Utils.getRemain(player, quest)));
			ItemStackUtils.addFlag(item, ItemFlag.HIDE_ATTRIBUTES);
			return item;
		}
	},
	
	STARTED {
		@Override
		public ItemStack getIcon(Player player, String quest, int point) {
			ItemStack item = new ItemStack(Material.BOOK_AND_QUILL);
			ItemStackUtils.setDisplayName(item, "§6§l" + quest);
			
			Quest q = QuestUtils.getQuestsPlugin().getQuest(quest);
			Quester qt = QuestUtils.getQuestsPlugin().getQuester(player.getUniqueId());
			
			List<String> lore = Lists.newArrayList();
			lore.add("§2Điểm: §f" + point);
			lore.add("§2Reset sau: §f" + Utils.format(Utils.getRemain(player, quest)));
			lore.add("§aTiến trình: §7" + QuestUtils.getStageInfo(q, qt.getCurrentQuests().get(q)));
			lore.add("§aYêu cầu:");
			
			Quests quests = (Quests) Bukkit.getPluginManager().getPlugin("Quests");
			Quester quester = quests.getQuester(player.getUniqueId());
			if (quester.getCurrentStage(q) != null) {
				for (CustomObjective o : quester.getCurrentStage(q).getCustomObjectives()) {
					if (o.getName().equalsIgnoreCase("Placeholder Change")) {
						double v = getReal(player, o, q);
						double r = getRequirement(player, o, q);
						LinkedList<Entry<String, Object>> l = quester.getCurrentStage(q).getCustomObjectiveData();
						for (Entry<String, Object> e : l) {
							if (e.getKey().equalsIgnoreCase("Add")) {
								double add = Double.valueOf(e.getValue().toString());
								lore.add("§e§o [" + (add - (r - v)) + "/" + add + "]");
								break;
							}
						}
					}
				}
			}
			
			qt.getObjectives(q, false).forEach(s -> lore.addAll(Utils.toList(ChatColor.stripColor(s), 25, "§e§o ")));
			lore.add("");
			ItemStackUtils.setLore(item, lore);
			
			return item;
		}
	};
	
	public abstract ItemStack getIcon(Player player, String quest, int point);
	
	public static final String KEY = "qs-qc-%placeholder%";
	
	private static double getRequirement(Player p, CustomObjective o, Quest quest) {
		PlayerData pd = PlayerDataAPI.get(p, "quests");
		for (Entry<String, String> e : Maps.newHashMap(pd.getDataMap()).entrySet()) {
			String k = e.getKey();
			String v = e.getValue();
			if (k.contains(KEY.replace("%placeholder%", ""))) {
				String plh = k.replace(KEY.replace("%placeholder%", ""), "");
				String qplh = o.getDataForPlayer(p, o, quest).get("Placeholder").toString();
				if (!plh.equalsIgnoreCase(qplh)) continue;
				double tv = Double.valueOf(v);
				return tv;
			}
		}
		return -1;
	}
	
	private static double getReal(Player p, CustomObjective o, Quest quest) {
		PlayerData pd = PlayerDataAPI.get(p, "quests");
		for (Entry<String, String> e : Maps.newHashMap(pd.getDataMap()).entrySet()) {
			String k = e.getKey();
			if (k.contains(KEY.replace("%placeholder%", ""))) {
				String plh = k.replace(KEY.replace("%placeholder%", ""), "");
				String qplh = o.getDataForPlayer(p, o, quest).get("Placeholder").toString();
				if (!plh.equalsIgnoreCase(qplh)) continue;
				double realv = Double.valueOf(PlaceholderAPI.setPlaceholders(p, plh));
				return realv;
			}
		}
		return -1;
	}
}
