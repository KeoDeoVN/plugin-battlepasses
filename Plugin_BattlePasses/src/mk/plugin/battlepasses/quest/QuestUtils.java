package mk.plugin.battlepasses.quest;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.blackvein.quests.Quest;
import me.blackvein.quests.Quester;
import me.blackvein.quests.Quests;
import mk.plugin.battlepasses.battlepass.BattlePass;
import mk.plugin.battlepasses.battlepass.BattlePasses;
import mk.plugin.battlepasses.mission.MissionGroup;
import mk.plugin.battlepasses.player.BPPlayer;
import mk.plugin.battlepasses.player.BPPlayers;

public class QuestUtils {

	public static ItemStack getIcon(Player player, String mID, String quest, int point) {
		return getStatus(mID, quest, player).getIcon(player, quest, point);
	}

	public static QuestStatus getStatus(String mID, String quest, Player player) {
		BPPlayer bpP = BPPlayers.get(player);
		if (bpP.getCompletedQuests(mID).contains(quest)) return QuestStatus.COOLDOWN;
		for (Quest q : getQuestsPlugin().getQuester(player.getUniqueId()).getCurrentQuests().keySet()) {
			if (q.getName().equalsIgnoreCase(quest)) return QuestStatus.STARTED;
		}
		return QuestStatus.COOLDOWN;
	}
	
	public static Quests getQuestsPlugin() {
		return Quests.getPlugin(Quests.class);
	}
	
	public static void failQuest(Player player, String questName) {
		if (getQuestsPlugin().getQuest(questName) == null) return;
		Quest quest = getQuestsPlugin().getQuest(questName);
		Quester quester = getQuestsPlugin().getQuester(player.getUniqueId());
		if (!quester.getCurrentQuests().containsKey(quest)) return;
		quest.failQuest(quester);
	}
	
	public static void takeQuest(Player player, String questName) {
		if (getQuestsPlugin().getQuest(questName) == null) return;
		Quest quest = getQuestsPlugin().getQuest(questName);
		Quester quester = getQuestsPlugin().getQuester(player.getUniqueId());
		quester.takeQuest(quest, false);
	}
	
	public static String getStageInfo(Quest quest, int stage) {
		int max = quest.getStages().size();
		return stage + "/" + max;
	}
	
	public static String getMission(String quest, String bpID) {
		BattlePass bp = BattlePasses.get(bpID);
		if (bp == null) return null;
		for (String mID : bp.getMissionGroups().keySet()) {
			MissionGroup mg = bp.getMissionGroup(mID);
			if (mg.getQuests().contains(quest)) return mID;
		}
		return null;
	}
	
}
