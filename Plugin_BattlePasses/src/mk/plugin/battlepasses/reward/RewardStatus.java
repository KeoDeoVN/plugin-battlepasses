package mk.plugin.battlepasses.reward;

import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import mk.plugin.battlepasses.battlepass.BattlePass;
import mk.plugin.battlepasses.battlepass.BattlePasses;
import mk.plugin.battlepasses.level.Level;
import mk.plugin.battlepasses.util.ItemStackUtils;
import mk.plugin.battlepasses.util.Utils;

public enum RewardStatus {
	
	CAN_GET {
		@Override
		public ItemStack getIcon(Player player, String bpID, int level) {
			ItemStack is = new ItemStack(Material.CONCRETE, 1, Utils.getColor(DyeColor.LIME));
			ItemStackUtils.setDisplayName(is, "§a§lCó thể nhận");
			
			BattlePass bp = BattlePasses.get(bpID);
			Reward r = bp.getLevel(level).getReward(bpID);
			ItemStackUtils.setLore(is, r.getDesc().stream().map(s -> s.replace("&", "§")).collect(Collectors.toList()));
			
			Level l = bp.getLevel(level);
			if (l.isSpecial()) ItemStackUtils.addEnchantEffect(is);
			
			return is;
		}
	},
	CANT_GET {
		@Override
		public ItemStack getIcon(Player player, String bpID, int level) {
			ItemStack is = new ItemStack(Material.CONCRETE, 1, Utils.getColor(DyeColor.RED));
			int point = BattlePasses.get().get(bpID).getLevel(level).getPointRequired(bpID);
			ItemStackUtils.setDisplayName(is, "§c§lChưa thể nhận");
			
			BattlePass bp = BattlePasses.get(bpID);
			Reward r = bp.getLevel(level).getReward(bpID);
			
			List<String> lore = Lists.newArrayList();
			lore.add("§7§oCần " + point + " điểm và nhận các quà phía trước");
			lore.add("");
			lore.addAll(r.getDesc().stream().map(s -> s.replace("&", "§")).collect(Collectors.toList()));
			ItemStackUtils.setLore(is, lore);
			
			Level l = bp.getLevel(level);
			if (l.isSpecial()) ItemStackUtils.addEnchantEffect(is);
			
			return is;
		}
	},
	GOTTEN {
		@Override
		public ItemStack getIcon(Player player, String bpID, int level) {
			ItemStack is = new ItemStack(Material.CONCRETE, 1, Utils.getColor(DyeColor.GREEN));
			ItemStackUtils.setDisplayName(is, "§2§lĐã nhận");
			
			BattlePass bp = BattlePasses.get(bpID);
			Reward r = bp.getLevel(level).getReward(bpID);
			ItemStackUtils.setLore(is, r.getDesc().stream().map(s -> s.replace("&", "§")).collect(Collectors.toList()));
			
			Level l = bp.getLevel(level);
			if (l.isSpecial()) ItemStackUtils.addEnchantEffect(is);
			
			return is;
		}
	};
	
	public abstract ItemStack getIcon(Player player, String bpID, int level);
	
}
