package mk.plugin.battlepasses.reward;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import mk.plugin.battlepasses.battlepass.BattlePass;
import mk.plugin.battlepasses.battlepass.BattlePasses;
import mk.plugin.battlepasses.level.Level;
import mk.plugin.battlepasses.main.MainBattlePasses;
import mk.plugin.battlepasses.player.BPPlayer;
import mk.plugin.battlepasses.player.BPPlayers;

public class Rewards {
	
	public static void reward(Player player, String bpID, int level) {
		if (checkStatus(player, bpID, level) != RewardStatus.CAN_GET) return;
		BattlePass bp = BattlePasses.get(bpID);
		BPPlayer data = BPPlayers.get(player);
		
		// Set data
		data.setLevel(level);
//		data.setPoint(0);
		BPPlayers.save(player, data);
		
		// Run rewards
		Bukkit.getScheduler().runTask(MainBattlePasses.get(), () -> {
			Level lv = bp.getLevel(level);
			lv.getReward(bpID).getcommands().forEach(cmd -> {
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd.replace("%player%", player.getName()));
			});
		});

	}
	
	public static RewardStatus checkStatus(Player player, String bpID, int level) {
		BPPlayer data = BPPlayers.get(player);
		if (!data.getBattlePassID().equals(bpID)) return null;
		BattlePass bp = BattlePasses.get(bpID);
		if (data.getLevel() >= level) return RewardStatus.GOTTEN;
		if (data.getLevel() < level - 1) return RewardStatus.CANT_GET;
		if (data.getLevel() == level - 1) {
			if (data.getPoint() < bp.getLevel(level).getPointRequired(bpID)) return RewardStatus.CANT_GET;
		}
		return RewardStatus.CAN_GET;
	}
	
	public static ItemStack getIcon(Player player, String bpID, int level) {
		return checkStatus(player, bpID, level).getIcon(player, bpID, level);
	}
	
}
