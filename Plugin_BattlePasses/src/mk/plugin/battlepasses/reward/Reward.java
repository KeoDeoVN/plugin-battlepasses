package mk.plugin.battlepasses.reward;

import java.util.List;

public class Reward {
	
	private List<String> commands;
	private List<String> desc;
	
	public Reward(List<String> commands, List<String> desc) {
		this.commands = commands;
		this.desc = desc;
	}
	
	public List<String> getcommands() {
		return this.commands;
	}
	
	public List<String> getDesc() {
		return this.desc;
	}
	
}
