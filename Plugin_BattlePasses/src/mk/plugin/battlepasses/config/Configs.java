package mk.plugin.battlepasses.config;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import mk.plugin.battlepasses.util.ItemStackUtils;

public class Configs {
	
	public static String GUI_TITLE;
	public static int GUI_SIZE;
	public static int GUI_ITEM_INFO_SLOT;
	public static int GUI_ITEM_REWARD_SLOT;
	public static int GUI_ITEM_POINT_BUY_SLOT;
	
	private static ItemStack INFO_ICON;
	private static ItemStack REWARD_ICON;
	private static ItemStack POINT_BUY_ICON;
	
	public static int MAX_POINT_BUY = 40;
	public static int PRICE_PER_POINT = 9;
	
	public static void reload(FileConfiguration config) {
		GUI_TITLE = config.getString("gui.title").replace("&", "§");
		GUI_SIZE = config.getInt("gui.size");
		GUI_ITEM_INFO_SLOT = config.getInt("gui.item.info.slot");
		GUI_ITEM_REWARD_SLOT = config.getInt("gui.item.reward.slot");
		GUI_ITEM_POINT_BUY_SLOT = config.getInt("gui.item.point-buy.slot");
		INFO_ICON = ItemBuilder.buildItem(config.getConfigurationSection("gui.item.info"));
		REWARD_ICON = ItemBuilder.buildItem(config.getConfigurationSection("gui.item.reward"));
		POINT_BUY_ICON = ItemBuilder.buildItem(config.getConfigurationSection("gui.item.point-buy"));
		MAX_POINT_BUY = config.getInt("option.max-point-buy");
		PRICE_PER_POINT = config.getInt("option.price-per-point");
	}
	
	public static ItemStack getPointBuyIcon(Map<String, String> placeholders) {
		ItemStack clone = POINT_BUY_ICON.clone();
		String name = ItemStackUtils.getName(POINT_BUY_ICON);
		List<String> lore = ItemStackUtils.getLore(POINT_BUY_ICON);
		for (Entry<String, String> e : placeholders.entrySet()) {
			name = name.replace(e.getKey(), e.getValue());
			for (int i = 0 ; i < lore.size() ; i++) {
				lore.set(i, lore.get(i).replace(e.getKey(), e.getValue()));
			}
		}
		ItemStackUtils.setDisplayName(clone, name);
		ItemStackUtils.setLore(clone, lore);
		
		return clone;
	}
	
	public static ItemStack getInfoIcon(Map<String, String> placeholders) {
		ItemStack clone = INFO_ICON.clone();
		String name = ItemStackUtils.getName(INFO_ICON);
		List<String> lore = ItemStackUtils.getLore(INFO_ICON);
		for (Entry<String, String> e : placeholders.entrySet()) {
			name = name.replace(e.getKey(), e.getValue());
			for (int i = 0 ; i < lore.size() ; i++) {
				lore.set(i, lore.get(i).replace(e.getKey(), e.getValue()));
			}
		}
		ItemStackUtils.setDisplayName(clone, name);
		ItemStackUtils.setLore(clone, lore);
		
		return clone;
	}
	
	public static ItemStack getRewardIcon(Map<String, String> placeholders) {
		ItemStack clone = REWARD_ICON.clone();
		String name = ItemStackUtils.getName(REWARD_ICON);
		List<String> lore = ItemStackUtils.getLore(REWARD_ICON);
		for (Entry<String, String> e : placeholders.entrySet()) {
			name = name.replace(e.getKey(), e.getValue());
			for (int i = 0 ; i < lore.size() ; i++) {
				lore.set(i, lore.get(i).replace(e.getKey(), e.getValue()));
			}
		}
		ItemStackUtils.setDisplayName(clone, name);
		ItemStackUtils.setLore(clone, lore);
		
		return clone;
	}
	
	
}
