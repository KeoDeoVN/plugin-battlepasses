package mk.plugin.battlepasses.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import mk.plugin.battlepasses.battlepass.BattlePasses;

public class PlayerCommand implements CommandExecutor  {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		
		if (sender instanceof Player) {
			Player player = (Player) sender;
			BattlePasses.open(player);
		}
		
		return false;
	}

}
