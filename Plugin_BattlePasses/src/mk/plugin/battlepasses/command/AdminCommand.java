package mk.plugin.battlepasses.command;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import mk.plugin.battlepasses.battlepass.BattlePass;
import mk.plugin.battlepasses.battlepass.BattlePasses;
import mk.plugin.battlepasses.gui.levels.GUILevel;
import mk.plugin.battlepasses.gui.missions.GUIMission;
import mk.plugin.battlepasses.main.MainBattlePasses;
import mk.plugin.battlepasses.manager.Timings;
import mk.plugin.battlepasses.player.BPPlayer;
import mk.plugin.battlepasses.player.BPPlayers;
import mk.plugin.battlepasses.quest.QuestStatus;
import mk.plugin.battlepasses.quest.QuestUtils;
import mk.plugin.battlepasses.util.Utils;
import mk.plugin.playerdata.storage.PlayerDataAPI;

public class AdminCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		
		try {
			
			if (args[0].equalsIgnoreCase("reload")) {
				MainBattlePasses.get().reloadConfigs();
				sender.sendMessage("Ok");
			}
			
			else if (args[0].equalsIgnoreCase("givepoint")) {
				String player = args[1];
				BPPlayer data = BPPlayers.get(player);
				data.setPoint(data.getPoint() + Integer.valueOf(args[2]));
				BPPlayers.save(player, data);
				sender.sendMessage("Done!");
			}
			
			else if (args[0].equalsIgnoreCase("set")) {
				String player = args[1];
				String bpID = args[2];
				
				// Check
				if (!BattlePasses.get().containsKey(bpID)) {
					sender.sendMessage("[BattlePass] Can't find battlepass id " + bpID);
					return false;
				}
				
				// Set
				BPPlayer bpP = BPPlayers.get(player);
				bpP.setBattlePassID(bpID, true);
				BPPlayers.save(player, bpP);
				
				sender.sendMessage("§aOkay, done!");
			}
			
			else if (args[0].equalsIgnoreCase("unset")) {
				String player = args[1];
				
				// Set
				BPPlayer bpP = BPPlayers.get(player);
				bpP.setBattlePassID(null, false);
				BPPlayers.save(player, bpP);
				
				sender.sendMessage("§aOkay, done!");
			}
			
			else if (args[0].equalsIgnoreCase("guilevel")) {
				Player player = Bukkit.getPlayer(args[1]);
				BPPlayer bpP = BPPlayers.get(player);
				if (bpP.getBattlePassID() == null) {
					sender.sendMessage("BattlePass null!");
					return false;
				}
				GUILevel.open(player, bpP.getBattlePassID());
			}
			
			else if (args[0].equalsIgnoreCase("guimission")) {
				Player player = Bukkit.getPlayer(args[1]);
				BPPlayer bpP = BPPlayers.get(player);
				
				if (bpP.getBattlePassID() == null) {
					sender.sendMessage("BattlePass null!");
					return false;
				}
				
				if (!Timings.isInSeason(bpP.getBattlePassID())) {
					sender.sendMessage("[BattlePass] Not in season battlepass " + bpP.getBattlePassID());
					return false;
				}
				GUIMission.open(player, bpP.getBattlePassID());
			}
			
			else if (args[0].equalsIgnoreCase("donemissions")) {
				Player player = Bukkit.getPlayer(args[1]);
				BPPlayer bpP = BPPlayers.get(player);
				String bpID = bpP.getBattlePassID();
				BattlePass bp = BattlePasses.get(bpID);
				bp.getMissionGroups().forEach((id, mg) -> {
					mg.getQuests().forEach(q -> {
						if (QuestUtils.getStatus(id, q, player) == QuestStatus.STARTED) {
							Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "questadmin finish " + player.getName() + " " + q);
						}
					});
				});
			}
			
			else if (args[0].equalsIgnoreCase("calmaxpoint")) {
				String bpID = args[1];
				sender.sendMessage(Utils.caculateMaxPoint(bpID) + "");
			}
			
			else if (args[0].equalsIgnoreCase("givepointall")) {
				int point = Integer.valueOf(args[1]);
				List<String> players = PlayerDataAPI.getPlayerList("battlepasses");
				players.forEach(p -> {
					BPPlayer bpP = BPPlayers.get(p);
					bpP.setPoint(bpP.getPoint() + point);
					BPPlayers.save(p, bpP);
				});
				sender.sendMessage("Ok, gave " + point + " points to " + players.size() + " players");
			}
			
			else if (args[0].equalsIgnoreCase("bypassquest")) {
				int point = Integer.valueOf(args[1]);
				String q = "";
				for (int i = 2 ; i < args.length ; i++) {
					q += args[i] + " ";
				}
				if (q.length() > 0) q = q.substring(0, q.length() - 1);
				String quest = q;
				List<String> players = PlayerDataAPI.getPlayerList("battlepasses");
				players.forEach(p -> {
					BPPlayer bpP = BPPlayers.get(p);
					bpP.addCompletedQuest(quest);
					bpP.setPoint(bpP.getPoint() + point);
					BPPlayers.save(p, bpP);
				});
				
				sender.sendMessage("Ok, finished " + quest + " for " + players.size() + " players");
			}
			
		}
		catch (ArrayIndexOutOfBoundsException e) {
			sendHelp(sender);
		}
		
		return false;
	}
	
	public void sendHelp(CommandSender sender) {
		sender.sendMessage("§2------------------------------");
		sender.sendMessage("§a/bpa reload: §7Reload plugin");
		sender.sendMessage("§a/bpa givepoint <player> <amount>: §7Give points to player");
		sender.sendMessage("§a/bpa set <player> <id>: §7Set battlepass for player");
		sender.sendMessage("§a/bpa unset <player>: §7Remove current battlepass for player");
		sender.sendMessage("§a/bpa calmaxpoint <id>: §7Calculate max points that player can get");
		sender.sendMessage("§a/bpa guilevel <player>: §7Open reward gui to player");
		sender.sendMessage("§a/bpa guimission <player>: §7Open mission gui to player");
		sender.sendMessage("§a/bpa donemissions <player>: §7Finish all current quests of player");
		sender.sendMessage("§a/bpa givepointall <amount>: §7Give points to all players (offline players included)");
		sender.sendMessage("§a/bpa bypassquest <name> <point>: §7Bypass quest for all players");
		sender.sendMessage("§2------------------------------");
	}

}
