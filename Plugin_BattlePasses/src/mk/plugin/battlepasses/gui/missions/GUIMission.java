package mk.plugin.battlepasses.gui.missions;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Maps;

import mk.plugin.battlepasses.battlepass.BattlePass;
import mk.plugin.battlepasses.battlepass.BattlePassData;
import mk.plugin.battlepasses.battlepass.BattlePasses;
import mk.plugin.battlepasses.config.Configs;
import mk.plugin.battlepasses.eco.EcoType;
import mk.plugin.battlepasses.gui.levels.GUILevel;
import mk.plugin.battlepasses.main.MainBattlePasses;
import mk.plugin.battlepasses.mission.MissionGroup;
import mk.plugin.battlepasses.player.BPPlayer;
import mk.plugin.battlepasses.player.BPPlayers;
import mk.plugin.battlepasses.quest.QuestUtils;
import mk.plugin.battlepasses.util.Utils;

public class GUIMission {
		
	public static void open(Player player, String bpID) {
		// Create inv
		Inventory inv = Bukkit.createInventory(new GUIMissionHolder(bpID), Configs.GUI_SIZE, Configs.GUI_TITLE);
		player.openInventory(inv);
		player.playSound(player.getLocation(), Sound.BLOCK_ENDERCHEST_OPEN, 1, 1);
		
		// Set 
		Bukkit.getScheduler().runTaskAsynchronously(MainBattlePasses.get(), () -> {
			// Set background
			for (int i = 0 ; i < inv.getSize() ; i++) inv.setItem(i, Utils.getBlackSlot());
			
			// Placeholders
//			BPPlayer bpP = BPPlayers.get(player);
//			BattlePass bp = BattlePasses.get(bpID);
//			Map<String, String> placeholders = Maps.newHashMap();
//			placeholders.put("%player_name%", player.getName());
//			placeholders.put("%player_point%", bpP.getPoint() + "");
//			placeholders.put("%bp_name%", bp.getName());
//			placeholders.put("%bp_time_remain%", Utils.format(Utils.getRemain(bpID)) + "");
//			placeholders.put("%player_point_buy_remain%", Configs.MAX_POINT_BUY - bpP.getPointBought() + "");
//			placeholders.put("%point_buy_price%", Configs.PRICE_PER_POINT + "");
//			
//			// Set config icon
//			inv.setItem(Configs.GUI_ITEM_INFO_SLOT, Configs.getInfoIcon(placeholders));
//			inv.setItem(Configs.GUI_ITEM_REWARD_SLOT, Configs.getRewardIcon(placeholders));
//			inv.setItem(Configs.GUI_ITEM_POINT_BUY_SLOT, Configs.getPointBuyIcon(placeholders));
//			
//			// Set quests' icon
//			BattlePassData bpData = BattlePasses.getData(bpID);
//			bpData.getActiveMissions().forEach((id, quests) -> {
//				MissionGroup mg = bp.getMissionGroup(id);
//				int i = -1;
//				for (int slot : mg.getSlots()) {
//					i++;
//					if (i >= quests.size()) break;
//					inv.setItem(slot, QuestUtils.getIcon(player, id, quests.get(i), mg.getPoint()));
//				}
//			});
		});
		
		// 
		new BukkitRunnable() {
			@Override
			public void run() {
				// Set
				if (player == null || !player.isOnline() || player.getOpenInventory() == null || !(player.getOpenInventory().getTopInventory().getHolder() instanceof GUIMissionHolder)) {
					this.cancel();
					return;
				}
				
				// Placeholders
				BPPlayer bpP = BPPlayers.get(player);
				BattlePass bp = BattlePasses.get(bpID);
				Map<String, String> placeholders = Maps.newHashMap();
				placeholders.put("%player_name%", player.getName());
				placeholders.put("%player_point%", bpP.getPoint() + "");
				placeholders.put("%player_level%", bpP.getLevel() + "");
				placeholders.put("%bp_name%", bp.getName());
				placeholders.put("%bp_time_remain%", Utils.format(Utils.getRemain(bpID)) + "");
				placeholders.put("%player_point_buy_remain%", Configs.MAX_POINT_BUY - bpP.getPointBought() + "");
				placeholders.put("%point_buy_price%", Configs.PRICE_PER_POINT + "");
				
				// Set config icon
				inv.setItem(Configs.GUI_ITEM_INFO_SLOT, Configs.getInfoIcon(placeholders));
				inv.setItem(Configs.GUI_ITEM_POINT_BUY_SLOT, Configs.getPointBuyIcon(placeholders));
				
				// Set quests' icon
				BattlePassData bpData = BattlePasses.getData(bpID);
				bpData.getActiveMissions().forEach((id, quests) -> {
					MissionGroup mg = bp.getMissionGroup(id);
					int i = -1;
					for (int slot : mg.getSlots()) {
						i++;
						if (i >= quests.size()) break;
						inv.setItem(slot, QuestUtils.getIcon(player, id, quests.get(i), mg.getPoint()));
					}
				});
			}
		}.runTaskTimerAsynchronously(MainBattlePasses.get(), 0, 10);
	}
	
	public static void onClick(InventoryClickEvent e) {
		Inventory inv = e.getInventory();
		if (!(inv.getHolder() instanceof GUIMissionHolder)) return;
		e.setCancelled(true);
		
		Player player = (Player) e.getWhoClicked();
		GUIMissionHolder holder = (GUIMissionHolder) inv.getHolder();
		player.playSound(player.getLocation(), Sound.BLOCK_STONE_BUTTON_CLICK_ON, 1, 1);
		
		// Check slot
		int slot = e.getSlot();
		if (slot == Configs.GUI_ITEM_REWARD_SLOT) {
			GUILevel.open(player, holder.getBattlePassID());
		}
		else if (slot == Configs.GUI_ITEM_POINT_BUY_SLOT) {
			BPPlayer bpP = BPPlayers.get(player);
			int remain = Configs.MAX_POINT_BUY - bpP.getPointBought();
			if (remain <= 0) {
				player.sendMessage("§cĐã đạt giới hạn mua điểm");
				return;
			}
			if (!EcoType.POINT.take(player, Configs.PRICE_PER_POINT)) {
				player.sendMessage("§cKhông đủ khả năng chi trả");
				return;
			}
			bpP.setPoint(bpP.getPoint() + 1);
			bpP.setPointBought(bpP.getPointBought() + 1);
			BPPlayers.save(player, bpP);
			player.sendMessage("§aMua điểm thành công");
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	

}
