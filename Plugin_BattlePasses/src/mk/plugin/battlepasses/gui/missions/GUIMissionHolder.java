package mk.plugin.battlepasses.gui.missions;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

public class GUIMissionHolder implements InventoryHolder {

	private String bpID;
	
	public GUIMissionHolder(String bpID) {
		this.bpID = bpID;
	}
	
	public String getBattlePassID() {
		return this.bpID;
	}
	
	@Override
	public Inventory getInventory() {
		return null;
	}

}
