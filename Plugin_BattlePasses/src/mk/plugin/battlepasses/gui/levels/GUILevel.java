package mk.plugin.battlepasses.gui.levels;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import mk.plugin.battlepasses.battlepass.BattlePass;
import mk.plugin.battlepasses.battlepass.BattlePasses;
import mk.plugin.battlepasses.config.Configs;
import mk.plugin.battlepasses.main.MainBattlePasses;
import mk.plugin.battlepasses.reward.RewardStatus;
import mk.plugin.battlepasses.reward.Rewards;
import mk.plugin.battlepasses.util.Utils;

public class GUILevel {
	
	public static void open(Player player, String bpID) {
		BattlePass bp = BattlePasses.get(bpID);
		List<Integer> levels = Lists.newArrayList(bp.getLevels().keySet());
		Collections.sort(levels);
		
		
		int invSize = getInvSize(levels.size());
		// Level slots
		Map<Integer, Integer> levelSlots = Maps.newHashMap();
		int count = -1;
		for (int slot : getRewardSlots(levels.size())) {
			count++;
			if (count >= levels.size()) break;
			levelSlots.put(slot, levels.get(count));
		}

		
		Inventory inv = Bukkit.createInventory(new GUILevelHolder(bpID, levelSlots), invSize, Configs.GUI_TITLE);
		player.openInventory(inv);
		player.playSound(player.getLocation(), Sound.BLOCK_ENDERCHEST_OPEN, 1, 1);
		
		// Set icons
		Bukkit.getScheduler().runTaskAsynchronously(MainBattlePasses.get(), () -> {
			for (int i = 0 ; i < inv.getSize() ; i++) inv.setItem(i, Utils.getBlackSlot());
			for (int i : getRewardSlots(levels.size())) inv.setItem(i, Utils.getBarrierSlot());
			levelSlots.forEach((slot, level) -> {
				inv.setItem(slot, Rewards.getIcon(player, bpID, level));
			});
		});
	}
	
	public static void onClick(InventoryClickEvent e) {
		Inventory inv = e.getInventory();
		if (!(inv.getHolder() instanceof GUILevelHolder)) return;
		e.setCancelled(true);
		Player player = (Player) e.getWhoClicked();
		player.playSound(player.getLocation(), Sound.BLOCK_STONE_BUTTON_CLICK_ON, 1, 1);
		
		if (e.getClickedInventory() != player.getOpenInventory().getTopInventory()) return;
		
		GUILevelHolder holder = (GUILevelHolder) inv.getHolder();
		String bpID = holder.getBattlePassID();
		Map<Integer, Integer> levelSlots = holder.getLevelSlots();
		
		int slot = e.getSlot();
		if (levelSlots.containsKey(slot)) {
			int level = levelSlots.get(slot);
			if (Rewards.checkStatus(player, bpID, level) != RewardStatus.CAN_GET) return;
			Rewards.reward(player, bpID, level);
			player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_LAUNCH, 1, 1);
			player.sendTitle("", "§a§l§oNHẬN QUÀ THÀNH CÔNG", 10, 40, 0);
			player.closeInventory();
			
			Bukkit.getScheduler().runTaskLater(MainBattlePasses.get(), () -> {
				GUILevel.open(player, holder.getBattlePassID());
			}, 20);
		}
	}
	
	public static List<Integer> getRewardSlots(int size) {
		List<Integer> list = Lists.newArrayList();
		
		int to = size % 7 != 0 ? size / 7 + 1 : size / 7;
		for (int i = 0 ; i < to ; i++) {
			for (int j = 0 ; j < 7 ; j++) {
				list.add((i + 1) * 9 + (j + 1));
			}
		}
		return list;
	}
	
	public static int getInvSize(int size) {
		int to = size % 7 != 0 ? size / 7 + 1 : size / 7;
		return (to + 2) * 9;
	}
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
