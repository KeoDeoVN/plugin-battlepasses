package mk.plugin.battlepasses.gui.levels;

import java.util.Map;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

public class GUILevelHolder implements InventoryHolder {
	
	private String bpID;
	private Map<Integer, Integer> levelSlots;
	
	public GUILevelHolder(String bpID, Map<Integer, Integer> levelSlots) {
		this.bpID = bpID;
		this.levelSlots = levelSlots;
	}
	
	public String getBattlePassID() {
		return this.bpID;
	}
	
	public Map<Integer, Integer> getLevelSlots() {
		return this.levelSlots;
	}
	
	public Integer getLevelFromSlot(int slot) {
		return this.levelSlots.getOrDefault(slot, 0);
	}

	@Override
	public Inventory getInventory() {
		return null;
	}
	
}
