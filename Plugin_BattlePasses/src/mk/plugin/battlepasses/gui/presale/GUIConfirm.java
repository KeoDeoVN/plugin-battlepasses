package mk.plugin.battlepasses.gui.presale;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import mk.plugin.battlepasses.main.MainBattlePasses;
import mk.plugin.battlepasses.util.ItemStackUtils;
import mk.plugin.battlepasses.util.Utils;

public class GUIConfirm {
	
	public Runnable r;
	
	public GUIConfirm(Runnable r) {
		this.r = r;
	}
	
	public Runnable getRunnable() {
		return this.r;
	}
	
	public void open(Player player) {
		Inventory inv = Bukkit.createInventory(new GCHolder(this), 9, "§0§lXác nhận thực hiện ?");
		player.openInventory(inv);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainBattlePasses.get(), () -> {
			for (int i = 0 ; i < inv.getSize() ; i++) inv.setItem(i, Utils.getBlackSlot());
			inv.setItem(2, getNo());
			inv.setItem(6, getYes());
		});
	}
	
	private ItemStack getNo() {
		ItemStack is = new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.RED));
		ItemStackUtils.setDisplayName(is, "§c§lKhông");
		return is;
	}
	
	private ItemStack getYes() {
		ItemStack is = new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.GREEN));
		ItemStackUtils.setDisplayName(is, "§a§lĐồng ý");
		return is;
	}
	
	public static void onClick(InventoryClickEvent e) {
		if (e.getInventory().getHolder() instanceof GCHolder == false) return;
		e.setCancelled(true);
		if (e.getClickedInventory() != e.getWhoClicked().getOpenInventory().getTopInventory()) return;
		
		
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		player.playSound(player.getLocation(), Sound.BLOCK_STONE_BUTTON_CLICK_ON, 1, 1);
		
		// No
		if (slot == 2) {
			player.closeInventory();
			player.sendMessage("§aĐã hủy!");
			return;
		}
		
		// Yes
		else if (slot == 6) {
			player.closeInventory();
			((GCHolder) e.getInventory().getHolder()).getGUI().getRunnable().run();
		}
	}
	
}

class GCHolder implements InventoryHolder {
	
	private GUIConfirm gc;
	
	public GCHolder(GUIConfirm gc) {
		this.gc = gc;
	}
	
	public GUIConfirm getGUI() {
		return this.gc;
	}
	
	@Override
	public Inventory getInventory() {
		return null;
	}
	
}