package mk.plugin.battlepasses.gui.presale;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import mk.plugin.battlepasses.battlepass.BattlePasses;
import mk.plugin.battlepasses.battlepass.presale.PreSaleStatus;
import mk.plugin.battlepasses.battlepass.presale.PreSales;
import mk.plugin.battlepasses.config.Configs;
import mk.plugin.battlepasses.main.MainBattlePasses;
import mk.plugin.battlepasses.player.BPPlayer;
import mk.plugin.battlepasses.player.BPPlayers;
import mk.plugin.battlepasses.util.Utils;

public class GUIPreSale {
	
	public static void open(Player player) {
		Inventory inv = Bukkit.createInventory(new GPSHolder(), 27, Configs.GUI_TITLE);
		player.openInventory(inv);
		player.playSound(player.getLocation(), Sound.BLOCK_ENDERCHEST_OPEN, 1, 1);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainBattlePasses.get(), () -> {
			for (int i = 0 ; i < inv.getSize() ; i++) inv.setItem(i, Utils.getBlackSlot());
		});
		
		new BukkitRunnable() {
			@Override
			public void run() {
				if (player.getOpenInventory() == null || !player.getOpenInventory().getTitle().equals(Configs.GUI_TITLE) ) {
					this.cancel();
					return;
				}
				BattlePasses.get().forEach((id, bp) -> {
					ItemStack icon = PreSales.getStatus(player, id).getIcon(id);
					inv.setItem(bp.getPreSale().getSlot(), icon);
				});
			}
		}.runTaskTimerAsynchronously(MainBattlePasses.get(), 0, 5);
	}
	
	public static void onClick(InventoryClickEvent e) {
		if (e.getInventory().getHolder() instanceof GPSHolder == false) return;
		e.setCancelled(true);
		if (e.getClickedInventory() != e.getWhoClicked().getOpenInventory().getTopInventory()) return;
		
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		BattlePasses.get().forEach((id, bp) -> {
			if (bp.getPreSale().getSlot() == slot) {
				PreSaleStatus status = PreSales.getStatus(player, id);
				if (status == PreSaleStatus.AVAILABLE) {
					// Check point
					if (bp.getPreSale().getEco().get(player) < bp.getPreSale().getPrice()) {
						player.sendMessage("§cKhông đủ khả năng chi trả");
						return;
					}
					
					new GUIConfirm(() -> {
						bp.getPreSale().cost(player);
						
						BPPlayer bpP = BPPlayers.get(player);
						bpP.setBattlePassID(id, true);
						BPPlayers.save(player, bpP);
						
						player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_LAUNCH, 1, 1);
						player.sendTitle("", "§aMua sổ sứ mệnh thành công!", 10, 20, 10);
						Bukkit.getScheduler().runTaskLater(MainBattlePasses.get(), () -> {
							BattlePasses.open(player);
						}, 20);
					}).open(player);
				}
			}
		});
	}
	
	
}

class GPSHolder implements InventoryHolder {
	
	@Override
	public Inventory getInventory() {
		return null;
	}
	
}
