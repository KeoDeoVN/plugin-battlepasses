package mk.plugin.battlepasses.mission;

import java.util.List;

public class MissionGroup {
	
	private long interval;
	private int randomAmount;
	private int point;
	private List<Integer> slots;
	private List<String> quests;

	public MissionGroup(long interval, int randomAmount, int point, List<Integer> slots, List<String> quests) {
		this.interval = interval;
		this.randomAmount = randomAmount;
		this.point = point;
		this.slots = slots;
		this.quests = quests;
	}
	
	public long getInterval() {
		return this.interval;
	}
	
	public int getRandomAmount() {
		return this.randomAmount;
	}
	
	public int getPoint() {
		return this.point;
	}
	
	public List<Integer> getSlots() {
		return this.slots;
	}
	
	public List<String> getQuests() {
		return this.quests;
	}
	
}
